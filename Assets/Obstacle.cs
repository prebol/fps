using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public Transform savezone;
    public Transform shootzone;
    public GameManager gm;
    public bool free;
    // Start is called before the first frame update
    void Start()
    {
        this.savezone = this.transform.GetChild(0).transform;
        this.shootzone = this.transform.GetChild(1).transform;
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        gm.obstacles.Add(this);  
        free = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }  
}
  