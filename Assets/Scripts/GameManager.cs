using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public List<Obstacle> obstacles;

    public List<EnemyMov> enemies;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void alert(Transform target) 
    {
        foreach (EnemyMov enemy in enemies) 
        {
            enemy.getalerted(target);
        }
    }

}
