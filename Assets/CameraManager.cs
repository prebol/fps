using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraManager : MonoBehaviour
{
    public GameObject zoomImage;
    public GameObject ammo;
    public GameObject[] cameraList;
    private int currentCamera;

    void Start()
    {/*
        currentCamera = 0;
        cameraList[1].gameObject.SetActive(false);*/
    }

    void Update()
    {
        
    }

    public void Zoom()
    {/*
        currentCamera = 1;
        cameraList[1].gameObject.SetActive(true);
        cameraList[0].gameObject.SetActive(false);*/
        ammo.SetActive(false);
        zoomImage.SetActive(true);
    }

    public void UnZoom()
    {/*
        currentCamera = 0;
        cameraList[0].gameObject.SetActive(true);
        cameraList[1].gameObject.SetActive(false);*/
        ammo.SetActive(true);
        zoomImage.SetActive(false);
    }
}
