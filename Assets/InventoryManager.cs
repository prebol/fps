using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryManager : MonoBehaviour
{
    private GameObject equipped;
    private Weapon actualWeapon;

    public GameObject ammo;
    private TextMeshProUGUI _actualAmmo;
    private TextMeshProUGUI _freeAmmo;

    public GameObject ak47;
    private Gun ak47Script;
    public GameObject sniper;
    private Gun sniperScript;
    public GameObject rocketlauncher;
    private Gun rocketlauncherScript;
    public GameObject shotgun;
    private Gun shotgunScript;

    private float _gunChargerAmmo;
    private float _gunFreeAmmo;

    void Start() {

        ak47Script = ak47.GetComponent<Gun>();
        sniperScript = sniper.GetComponent<Gun>();
        rocketlauncherScript = rocketlauncher.GetComponent<Gun>();
        shotgunScript = shotgun.GetComponent<Gun>();
        _actualAmmo = ammo.transform.Find("Charger").transform.GetComponent<TextMeshProUGUI>();
        _freeAmmo = ammo.transform.Find("AllBullets").transform.GetComponent<TextMeshProUGUI>();
        this.equipped = ak47;
    }

    void Update()
    {
        UpdateHUD();
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            switch (actualWeapon)
            {
                case Weapon.SNIPER:
                    sniper.SetActive(false);
                    GameObject arm = sniper.transform.GetChild(1).gameObject;
                    arm.transform.parent = ak47.transform;
                    break;

                case Weapon.BOW:
                    break;

                case Weapon.SHOTGUN:
                    shotgun.SetActive(false);
                    GameObject arm3 = shotgun.transform.GetChild(1).gameObject;
                    arm3.transform.parent = ak47.transform;
                    break;

                case Weapon.BAZOOKA:
                    rocketlauncher.SetActive(false);
                    GameObject arm4 = rocketlauncher.transform.GetChild(1).gameObject;
                    arm4.transform.parent = ak47.transform;
                    break;
            }
            if (this.actualWeapon != Weapon.AK47)
            {
                ak47.SetActive(true);
                this.equipped = ak47;
                this.actualWeapon = Weapon.AK47;
                ak47Script.ChangeCameraRecoil();
                ak47Script.animator.Play("ak47_Idle");
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            switch (actualWeapon)
            {
                case Weapon.AK47:
                    ak47.SetActive(false);
                    GameObject arm = ak47.transform.GetChild(1).gameObject;
                    arm.transform.parent = shotgun.transform;
                    break;

                case Weapon.BOW:
                    break;

                case Weapon.SNIPER:
                    sniper.SetActive(false);
                    GameObject arm2 = sniper.transform.GetChild(1).gameObject;
                    arm2.transform.parent = shotgun.transform;
                    break;

                case Weapon.BAZOOKA:
                    rocketlauncher.SetActive(false);
                    GameObject arm4 = rocketlauncher.transform.GetChild(1).gameObject;
                    arm4.transform.parent = shotgun.transform;
                    break;
            }
            if (this.actualWeapon != Weapon.SHOTGUN)
            {
                shotgun.SetActive(true);
                this.equipped = shotgun;
                this.actualWeapon = Weapon.SHOTGUN;
                shotgunScript.ChangeCameraRecoil();
                shotgunScript.animator.Play("SHG_Idle");
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {   
            switch (actualWeapon) 
            {
                case Weapon.AK47:
                    ak47.SetActive(false);
                    GameObject arm = ak47.transform.GetChild(1).gameObject;
                    arm.transform.parent = sniper.transform;
                    break;

                case Weapon.BOW:
                    break;

                case Weapon.SHOTGUN:
                    shotgun.SetActive(false);
                    GameObject arm3 = shotgun.transform.GetChild(1).gameObject;
                    arm3.transform.parent = sniper.transform;
                    break;

                case Weapon.BAZOOKA:
                    rocketlauncher.SetActive(false);
                    GameObject arm4 = rocketlauncher.transform.GetChild(1).gameObject;
                    arm4.transform.parent = sniper.transform;
                    break;
            }
            if (this.actualWeapon != Weapon.SNIPER)
            {
                sniper.SetActive(true);
                this.equipped = sniper;
                this.actualWeapon = Weapon.SNIPER;
                sniperScript.ChangeCameraRecoil();
                sniperScript.animator.Play("awp_Idle");
            }
        }else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            switch (actualWeapon)
            {
                case Weapon.AK47:
                    ak47.SetActive(false);
                    GameObject arm = ak47.transform.GetChild(1).gameObject;
                    arm.transform.parent = rocketlauncher.transform;
                    break;

                case Weapon.BOW:
                    break;
                     
                case Weapon.SHOTGUN:
                    break;

                case Weapon.SNIPER:
                    sniper.SetActive(false);
                    GameObject arm4 = sniper.transform.GetChild(1).gameObject;
                    arm4.transform.parent = rocketlauncher.transform;
                    break;
            }
            if (this.actualWeapon != Weapon.BAZOOKA)
            {
                rocketlauncher.SetActive(true);
                this.equipped = rocketlauncher;
                this.actualWeapon = Weapon.BAZOOKA;
                rocketlauncherScript.ChangeCameraRecoil();
                rocketlauncherScript.animator.Play("RL_Idle");
            }
        }
    } 

    private void UpdateHUD() {
        switch (actualWeapon)
        {
            case Weapon.AK47:
                _actualAmmo.text = "" + ak47Script.getActualChargerAmmo();   //Set Current Bullets
                _freeAmmo.text = "" + ak47Script.getFreeAmmo();   //Set Rest Of Free Bullets in the inventory
                break;

            case Weapon.SNIPER:
                _actualAmmo.text = "" + sniperScript.getActualChargerAmmo();   //Set Current Bullets
                _freeAmmo.text = "" + sniperScript.getFreeAmmo();   //Set Rest Of Free Bullets in the inventory
                break;

            case Weapon.BOW:
                break;

            case Weapon.SHOTGUN:
                _actualAmmo.text = "" + shotgunScript.getActualChargerAmmo();   //Set Current Bullets
                _freeAmmo.text = "" + shotgunScript.getFreeAmmo();   //Set Rest Of Free Bullets in the inventory
                break;

            case Weapon.BAZOOKA:
                _actualAmmo.text = "" + rocketlauncherScript.getActualChargerAmmo();   //Set Current Bullets
                _freeAmmo.text = "" + rocketlauncherScript.getFreeAmmo();   //Set Rest Of Free Bullets in the inventory
                break;
        }
    }
}

public enum Weapon
{
    AK47,
    SNIPER,
    BOW,
    SHOTGUN,
    BAZOOKA
}
