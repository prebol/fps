using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BulletHole : MonoBehaviour
{
    private MeshRenderer mr;

    private void Awake()
    {
        this.mr = this.GetComponent<MeshRenderer>();
    }

    void Start()
    {
        StartCoroutine(Disappear());
    }

    void Update()
    {
        
    }

    IEnumerator Disappear()
    {
        Color transparent = new Color(0, 0, 0, 0);
        yield return new WaitForSeconds(10);
        this.mr.material.color = transparent;
        Destroy(this.gameObject);
    }

}
