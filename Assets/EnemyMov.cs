using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMov : MonoBehaviour
{
    public Vector3 posinicial;
    public Vector3 poseyesini;
    public Quaternion rot;
    public Quaternion roteyes;

    public WeaponObject weapon;

    public float beforelastshoot=5f;
    public float beforegiveup=5f;
    public float timer=0f;
    public float timerforshoot=0f;
    public float timerbetweenshoots = 0f;
    public Transform target;            // Assign the target in the inspector
    public float speed;                 // The speed at which the enemy moves
    public float range;                 // The distance at which the enemy detects the target
    
    public Transform eyes;              // The transform where the raycast is originating from
    public float fireRange;             // The distance at which the enemy can fire at the target
    public float fireRate;              // The time between shots
    
    public LayerMask layer;
    public float fireTimer;            // The timer for the fire rate

    public Obstacle cov=null;
    public Vector3 lastpos;

    public float detectzone;
    public GameEvent alertevent;
    //Old booleans for state machine using ifs >.<
    /*
    public bool searchcov = false;
    public bool searching=false;
    public bool targetDetected=false;
    public bool desprotecting=false;
    public bool volver = false;
    */
    public GameManager gm;

    CharacterController ch;
    public float vida = 100;

    public soldierstate state;
    public Transform ragdoll;
    private void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        ch=this.GetComponent<CharacterController>();
        this.posinicial = this.transform.position;
        rot = this.transform.rotation;
        roteyes = eyes.rotation;
        state = soldierstate.patrol;
        poseyesini = eyes.transform.position;
        ragdoll = transform.GetChild(2).transform; 
        gm.enemies.Add(this);
    }
    void Update()
    {
        switch (state) 
        {
            case soldierstate.patrol:
                // Fire a raycast
                RaycastHit hit;
                if (Physics.Raycast(eyes.position, eyes.forward, out hit, range, layer))
                {

                    // Check if the raycast hit the target
                    if (hit.transform.tag == "Player")
                    {
                        print("no iker");
                        // Set the targetDetected flag to true
                        target = hit.transform;
                        state = soldierstate.Covered;
                        Debug.DrawRay(eyes.position, eyes.forward * range, Color.green);
                        alertevent.Raise();
                    }
                }
                else
                {
                    Debug.DrawRay(eyes.position, eyes.forward * range, Color.red);
                }
                break;
            case soldierstate.Covered:
                float distance = Vector3.Distance(transform.position, target.position);
                //print(distance);
                print(range);
                if (distance < range)
                {
                    if (cov == null)
                    {
                        calcnearestobstacle();
                        state = soldierstate.covering;
                        this.GetComponent<NavMeshAgent>().destination = cov.savezone.position;
                    }
                    else    
                    {

                        if (timerforshoot < beforelastshoot)
                        {
                            timerforshoot += Time.deltaTime;
                            print(timer);
                            print(fireRange);
                            if (distance < fireRange)
                            {
                                print(fireTimer);
                                print(" no Iker");
                                if (fireTimer <= 0)
                                {
                                    this.GetComponent<NavMeshAgent>().destination = cov.shootzone.position;
                                    state = soldierstate.descovering;

                                }
                                else
                                {
                                    fireTimer -= Time.deltaTime;
                                }
                            }  

                            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

                        }
                        else
                        {
                            calcnearestobstacle();
                            this.GetComponent<NavMeshAgent>().destination = cov.savezone.position;
                            timerforshoot = 0;
                            state = soldierstate.covering;
                        }

                    }
                }
                else
                {

                    this.lastpos = target.transform.position;
                    this.GetComponent<NavMeshAgent>().destination = lastpos;
                    state =soldierstate.searching;
                    cov.free = true;  
                    cov = null;     

                }
                break;
            case soldierstate.covering:
                if (Mathf.Round(this.transform.position.x) == Mathf.Round(cov.savezone.transform.position.x) && Mathf.Round(this.transform.position.z) == Mathf.Round(cov.savezone.transform.position.z))
                {
                    state = soldierstate.Covered;
                }
                break;
            case soldierstate.descovering:

                this.transform.LookAt(target.transform);
                eyes.LookAt(target.transform);

                if (Mathf.Round(this.transform.position.x) == Mathf.Round(cov.shootzone.position.x) && Mathf.Round(this.transform.position.z) == Mathf.Round(cov.shootzone.position.z))
                {
                    if (timer >= 3)
                    {
                        fireTimer = fireRate;
                        this.GetComponent<NavMeshAgent>().destination = cov.savezone.position;
                        state = soldierstate.covering;
                        timer = 0;
                    }
                    else 
                    {
                        print(timerforshoot);
                        timer += Time.deltaTime;
                        Debug.DrawRay(eyes.position, eyes.forward * range, Color.red);
                        if (timerbetweenshoots > weapon.fireRate / 10)
                        {
                            print("entroen disparos");
                            timerbetweenshoots = 0f;
                            RaycastHit hit4shoot;
                            if (Physics.Raycast(eyes.position, eyes.forward, out hit4shoot, fireRange, layer))
                            {

                                if (hit4shoot.transform.tag == target.transform.tag)
                                {
                                    hit4shoot.transform.gameObject.GetComponent<PlayerMovement>().hp -= weapon.damage;
                                    print("Disparo");

                                    timerforshoot = 0;

                                }
                            }
                            else print("No veo nadie");

                            
                        }
                        else
                        {
                            timerbetweenshoots += Time.deltaTime;
                        }
                    }
                }
                break;
            case soldierstate.searching:


                if (Mathf.Round(this.transform.position.x) == Mathf.Round(lastpos.x) && Mathf.Round(this.transform.position.z) == Mathf.Round(lastpos.z))
                {
                    print("iker");
                    eyes.transform.LookAt(target.position);
                    RaycastHit hit4reenconter;
                    if (Physics.Raycast(eyes.position, eyes.forward, out hit4reenconter, fireRange))
                    {

                        if (hit4reenconter.transform.tag == target.transform.tag)
                        {

                            print("Encontrado");
                            state = soldierstate.Covered;
                            //alertevent.Raise();
                        }
                        else
                        {
                            this.GetComponent<NavMeshAgent>().destination = posinicial;
                            state = soldierstate.returning;

                            target = null;
                        }
                    }
                    else
                    {
                        this.GetComponent<NavMeshAgent>().destination = posinicial;
                        state = soldierstate.returning;

                        target = null;
                    }
                }
                break;
            case soldierstate.returning: 

                if (Mathf.Round(this.transform.position.x) == Mathf.Round(posinicial.x) && Mathf.Round(this.transform.position.z) == Mathf.Round(posinicial.z))
                {
                    
                    eyes.SetPositionAndRotation(poseyesini, roteyes);    
                    transform.SetPositionAndRotation(posinicial, rot); 
                    state = soldierstate.patrol;  
                }    
                break;
        } 
        /*  
        if (volver) 
        {
            this.GetComponent<NavMeshAgent>().destination = posinicial;


            if (this.transform.position.x == posinicial.x && this.transform.position.z == posinicial.z)
            {
                eyes.Rotate(rot);
                volver = false;
            }
        }
        // Check if the target is already detected
        else if (!targetDetected)
        {
            print("iker");
            // Fire a raycast
            RaycastHit hit;
            if (Physics.Raycast(eyes.position, eyes.forward, out hit, range, layer))
            {
                
                // Check if the raycast hit the target
                if (hit.transform.tag == "Player")
                {
                    print("no iker");
                    // Set the targetDetected flag to true
                    targetDetected = true;
                    target = hit.transform;
                    Debug.DrawRay(eyes.position, eyes.forward * range, Color.green);
                }
            }
            else   
            {
                Debug.DrawRay(eyes.position, eyes.forward * range, Color.red);
            }
           
        }
        else
        {
            if (searchcov)
            {

                this.GetComponent<NavMeshAgent>().destination = cov.savezone.position;

                


                if (this.transform.position.x == cov.savezone.transform.position.x && this.transform.position.z == cov.savezone.transform.position.z)
                {
                    searchcov = false;
                }

            }
            else if (searching)
            {
                this.GetComponent<NavMeshAgent>().destination = lastpos;
                if (this.transform.position.x == lastpos.x && this.transform.position.z == lastpos.z)
                {
                    print("iker");
                    eyes.transform.LookAt(target.position);
                    RaycastHit hit;
                    if (Physics.Raycast(eyes.position, eyes.forward, out hit, fireRange))
                    {

                        if (hit.transform.tag == target.transform.tag)
                        {

                            print("Encontrado");
                            searching = false;
                        }
                        else
                        {
                            searching = false;
                            targetDetected = false;
                            searchcov = false;
                            desprotecting = false;
                            volver = true;
                            target = null;
                            cov = null;
                        }
                    }
                    else
                    {
                        searching = false;
                        targetDetected = false;
                        searchcov = false;
                        desprotecting = false;
                        volver = true;
                        target = null;
                        cov = null;
                    }

                }
            }
            else if (desprotecting) 
            {
                
                this.transform.LookAt(target.transform);

                this.GetComponent<NavMeshAgent>().destination = cov.shootzone.position;
                
                if (this.transform.position.x == cov.shootzone.position.x && this.transform.position.z == cov.shootzone.position.z)
                {
                    if (timer >= 3)
                    {
                        
                        fireTimer = fireRate;  
                        searchcov = true;
                        desprotecting = false;
                        timer = 0;
                    }
                    else
                    {
                        print(timerforshoot); 
                        timer += Time.deltaTime;
                        if (timerbetweenshoots > weapon.fireRate/10)
                        {
                            timerbetweenshoots = 0f;
                            RaycastHit hit;
                            if (Physics.Raycast(eyes.position, eyes.forward, out hit, fireRange))
                            {  

                                if (hit.transform.tag == target.transform.tag)
                                {
                                    hit.transform.gameObject.GetComponent<PlayerMovement>().hp -= weapon.damage;
                                    print("Disparo");  
                                    
                                    timerforshoot = 0;
                                      
                                }  
                            }

                            Debug.DrawRay(eyes.position, eyes.forward * range, Color.red);
                        }
                        else 
                        {
                            timerbetweenshoots += Time.deltaTime;
                        }
                    }
                }
            }
            else
            {
                print(timer);
                float distance = Vector3.Distance(transform.position, target.position);
                //print(distance);
                print(range);
                if (distance < range)
                {
                    print("iker");
                    if (cov == null)
                    {
                        calcnearestobstacle();
                        searchcov = true;
                    }
                    else
                    {

                        if (timerforshoot < beforelastshoot)
                        {
                            timerforshoot += Time.deltaTime;
                            print(timer);
                            print(fireRange);
                            if (distance < fireRange)
                            {
                                print(fireTimer);
                                print(" no Iker");
                                if (fireTimer <= 0)
                                {
                                    desprotecting = true;
                                }
                                else
                                {

                                    fireTimer -= Time.deltaTime;
                                }
                            }

                            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

                        }
                        else
                        {
                            calcnearestobstacle();
                            timerforshoot = 0;
                            searchcov = true;
                        }

                    }
                }
                else
                {
                    this.lastpos = target.transform.position;
                    searching = true;
                }
            }
            
        }
        */
    }

    private void calcnearestobstacle()
    {
        int pito = 0;
        print(gm.obstacles.Count);
        foreach (Obstacle obs in gm.obstacles) 
        {
            pito++;
            print("Objeto " + pito);
            if (cov == null && obs.free)
            {
                this.cov = obs;
                obs.free = false;
            }
            else if(cov!=null &&obs.free)
            {
                
                float distanceobs = Vector3.Distance(obs.transform.position, target.position);
                float distancecov = Vector3.Distance(transform.position, target.position);

                if (this.cov.transform!=obs.transform && distanceobs<distancecov) 
                {  
                    cov.free = true;
                    this.cov = obs;
                    obs.free = false;  
                }
            }
        }

    }
    public void damage(float dmg) 
    {
        this.vida -= dmg;
        if (this.vida<=0) 
        {
            this.gameObject.transform.GetChild(1).transform.gameObject.SetActive(false);
            this.GetComponent<NavMeshAgent>().enabled = false;
            this.GetComponent<CharacterController>().enabled = false;
            ragdoll.gameObject.SetActive(true);
        }
    }
    public void getalerted(Transform newtarget) 
    {
        float distance = Vector3.Distance(this.transform.position, newtarget.position);

        if (state==soldierstate.patrol && distance<this.detectzone) 
        {

            target = newtarget;
            state = soldierstate.Covered;

        }
    }
    public enum soldierstate 
    { 
        patrol, searching, covering, descovering, returning, Covered
    }
}

