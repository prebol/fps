using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    private float radius = 10;
    private float power = 20;
    private Rigidbody rb;

    private void Awake()
    {
        this.rb = this.GetComponent<Rigidbody>();
    }

    void Start()
    {
        
    }

    void Update()
    {
        //print(this.rb.velocity);
    }

    private void OnEnable()
    {
        this.rb.velocity = transform.right * 30;
    }

    private void OnDisable() 
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
        {
            print(collision.gameObject.name);

            this.rb.velocity = Vector3.zero;

            Vector3 explosionPos = transform.position;
            Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(power, explosionPos, radius, 3, ForceMode.Impulse);
            }

            this.gameObject.SetActive(false);
        }
    }
}
