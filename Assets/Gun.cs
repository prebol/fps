using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Gun : MonoBehaviour
{
    public WeaponObject info;

    private Weapon weapon;

    public CameraManager brain;
     
    public GameObject hitmarker; 
    public AudioClip hitmarkerSound;
    private AudioClip gunShotSound;
    private AudioSource audioSource;

    private float damage;
    private float fireRate;
    private float actualChargerAmmo;
    private float maxChargerCapacity;
    private float freeAmmo;
    private float maxAmmo;

    private bool isReloading = false;
    private bool isRunning = false;
    private bool isShooting = false;

    public Camera fpsCam;
    public ParticleSystem shootParticle;

    public Animator animator;

    public GameObject playerController;
    private PlayerMovement _playerMovement;

    private float nextTimeToFire = 0f;

    public GameObject recoilGameObject;

    private float recoilX;
    private float recoilY;
    private float recoilZ;
    private float snappiness;
    private float returnSpeed;

    private bool zoom = false;
    private float previousSens;

    private bool rocketReady = true;
    private bool aimingRocket = false;
    public GameObject poolRocket;

    public GameObject bulletHole;

    /* 
    [Range(0,10)]public float recoilY;
    [Range(0,5)]public float recoilX;

    public float currentRecoilY;
    public float currentRecoilX;

    private float maxRecoilTime = 3;
    private float timeShooting;*/

    public Recoil recoilScript;

    private void Awake()
    {
        this.weapon = info.type;
        this.damage = info.damage;
        this.fireRate = info.fireRate;
        this.actualChargerAmmo = info.actualChagerAmmo;
        this.maxChargerCapacity = info.maxChargerCapacity;
        this.freeAmmo = info.MaxAmmo;
        this.maxAmmo = info.MaxAmmo;
        this.recoilX = info.recoilX;
        this.recoilY = info.recoilY;
        this.snappiness = info.snappiness;
        this.returnSpeed = info.returnSpeed;
        this.gunShotSound = info.gunShotSound;
        InstantiateAudio();
        _playerMovement = playerController.GetComponent<PlayerMovement>();
    }

    void Start()
    {
        //recoilScript = this.transform.Find("CameraRotation/CameraRecoil").GetComponent<Recoil>();
    }

    private void InstantiateAudio() {
        audioSource = gameObject.AddComponent<AudioSource>();
    }

    public void playSound(AudioClip clip) {
        audioSource.clip = clip;
        audioSource.PlayOneShot(clip);
    }
    void Update()
    {
        animator.SetBool("isRunning", _playerMovement.isRunning);

        if (weapon == Weapon.BAZOOKA)
        {
            if (!rocketReady)
            {
                if (Time.time >= nextTimeToFire && (actualChargerAmmo > 0))
                {
                    GameObject Rocket = transform.GetChild(0).gameObject;

                    Rocket.SetActive(true);

                }
            }
        }

        //DISPARAR
        if (Input.GetMouseButton(0) && Time.time >= nextTimeToFire && (actualChargerAmmo > 0)) {
            isShooting = true;
            nextTimeToFire = Time.time + 1f / fireRate;
            if (weapon == Weapon.BAZOOKA)
            {
                if (aimingRocket)
                {
                    ShootRocket();
                }
            }
            if (weapon == Weapon.SHOTGUN)
            {
                ShootShotgun();
            }
            else
            {
                Shoot();
            }
        } else if (Input.GetMouseButton(0) && Time.time < nextTimeToFire) {
            //aun quiere disparar pero no puede por la cadencia, se detecta como que aun esta disparando
        } else {
            //no esta disparando
            isShooting = false;
        }

        if (Input.GetMouseButtonDown(1))
        {
            switch (weapon)
            {
                case Weapon.SNIPER:
                    if (!zoom)
                    {
                        Zoom();
                    }
                    else
                    {
                        UnZoom();
                    }
                    break;
            }
        }

        if (Input.GetMouseButton(1))
        {
            switch (weapon)
            {
                case Weapon.BAZOOKA:
                    if (!aimingRocket)
                    {
                        StartCoroutine(AimTransition());
                    }
                    else
                    {
                        animator.Play("RL_Aim");
                    }
                    break;
            }
        }
        if (Input.GetMouseButtonUp(1))
        {
            switch (weapon)
            {
                case Weapon.BAZOOKA:
                    if (aimingRocket)
                    {
                        StartCoroutine(UnaimTransition());
                    }
                    break;
            }
        }

        //RECARGAR
        if (Input.GetKeyDown(KeyCode.R) && !isReloading) {
            if (actualChargerAmmo != maxChargerCapacity) {
                StartCoroutine(ReloadGun());
            }
        }
    }

    void Shoot()
    {
        shootParticle.Play();
        playSound(gunShotSound);

        // animator.Play("recoil");
        StartCoroutine(gunRecoil());

        actualChargerAmmo--;
 
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit))
        {
            if (hit.transform.CompareTag("enemy"))
            {
                hit.transform.GetComponent<EnemyMov>().damage(damage);
                // hit.transform.GetComponent<Target>()?.takeDamage(damage);   //deals damage to entity
                StopCoroutine(hideHitmarker()); //Stops the previous running coroutine
                StartCoroutine(hideHitmarker());    //Starts new coroutine
                playSound(hitmarkerSound);
                //print("RAYCAST HAS HIT: " + hit.transform.name);
            }
            if (hit.transform.CompareTag("Obstacle") || hit.transform.CompareTag("map"))
            {
                GameObject iker = Instantiate(bulletHole, hit.point, Quaternion.LookRotation(hit.normal));
                iker.transform.right = hit.normal;
            }
        }

        recoilScript.RecoilFire();

        if (zoom)
        {
            UnZoom();
        }

    }

    void ShootShotgun()
    {
        shootParticle.Play();
        playSound(gunShotSound);

        // animator.Play("recoil");
        StartCoroutine(gunRecoil());

        actualChargerAmmo--;

        recoilScript.RecoilFire();

        for (int i = 0; i < 8; i++)  
        {
            RaycastHit hit;
            float xspread = Random.Range(-0.3f, 0.3f);
            float yspread = Random.Range(-0.05f, 0.05f);
            //print("Bala: "+i+" X: "+xspread + " Y: " + yspread);
            if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward + new Vector3(xspread, yspread, 0), out hit))
            {
                //print("Golpe: "+hit.point);
                if (hit.transform.CompareTag("enemy"))
                {
                    hit.transform.GetComponent<Target>()?.takeDamage(damage);   //deals damage to entity
                    StopCoroutine(hideHitmarker()); //Stops the previous running coroutine
                    StartCoroutine(hideHitmarker());    //Starts new coroutine
                    playSound(hitmarkerSound);
                    //print("RAYCAST HAS HIT: " + hit.transform.name);
                }
                if (hit.transform.CompareTag("Obstacle"))
                {
                    GameObject iker = Instantiate(bulletHole, hit.point, Quaternion.LookRotation(hit.normal));
                    iker.transform.right = hit.normal;
                }
            }
        }
    }

    void ShootRocket()
    {
        GameObject Rocket = transform.GetChild(0).gameObject;

        Rocket.SetActive(false);

        shootParticle.Play();
        playSound(gunShotSound);

        StartCoroutine(gunRecoil());

        actualChargerAmmo--;

        for (int i = 0; i < poolRocket.transform.childCount; i++)
        {
            if (!poolRocket.transform.GetChild(i).gameObject.activeSelf)
            {
                poolRocket.transform.GetChild(i).gameObject.transform.rotation = this.transform.GetChild(0).transform.rotation;
                poolRocket.transform.GetChild(i).gameObject.transform.position = new Vector3(this.transform.GetChild(0).position.x, this.transform.GetChild(0).position.y, this.transform.GetChild(0).position.z);
                poolRocket.transform.GetChild(i).gameObject.SetActive(true);
                break;
            }
        }

        recoilScript.RecoilFire();

        rocketReady = false;
    }


    IEnumerator AimTransition()
    {
        animator.Play("RL_AimTransition");
        yield return new WaitForSeconds(1f);
        if (Input.GetMouseButton(1))
        {
            aimingRocket = true;
        }
        else
        {
            StartCoroutine(UnaimTransition());
        }
    }

    IEnumerator UnaimTransition()
    {
        aimingRocket = false;
        animator.Play("RL_UnaimTransition");
        yield return new WaitForSeconds(1f);
        animator.Play("RL_Idle");
    }

    public void Zoom()
    {
        brain.Zoom();
        zoom = true;
        fpsCam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView = 15;
        previousSens = fpsCam.GetComponent<MouseLook>().mouseSensitivity; 
        fpsCam.GetComponent<MouseLook>().mouseSensitivity = previousSens/10;
    }

    public void UnZoom()
    {
        brain.UnZoom();
        zoom = false;
        fpsCam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView = 90;
        fpsCam.GetComponent<MouseLook>().mouseSensitivity = previousSens;
    }

    IEnumerator ReloadGun() {
        isReloading = true;
        animator.SetBool("isReloading", true);
        yield return new WaitForSeconds(1.5f);
        animator.SetBool("isReloading", false);
        Reload();
        isReloading = false;
    }

    void Reload() {
        float bullets = maxChargerCapacity - actualChargerAmmo;
        freeAmmo-= bullets;
        actualChargerAmmo = maxChargerCapacity;
        //TODO - Cuando quedan pocas balas en el inventario que sume a las actuales la cantidad correcta y no se pongan en negativo.
    }

    /*
    public void CalculoRecoil()
    {
        currentRecoilX = ((Random.value - 0.5f) / 2) * recoilX;
        currentRecoilY = ((Random.value - 0.5f) / 2) * (timeShooting >= maxRecoilTime ? recoilY /4 :recoilY);

    }*/

    public void ChangeCameraRecoil()
    {
        this.recoilGameObject.GetComponent<Recoil>().ChangeRecoilStats(recoilX, recoilY, recoilZ, snappiness, returnSpeed);
    }

    public float getActualChargerAmmo()
    {
        return this.actualChargerAmmo;
    }
    public float getFreeAmmo()
    {
        return this.freeAmmo;
    }
    
    public bool isShootingFunc()
    {
        return this.isShooting;
    }

    public bool isReloadingFunc()
    {
        return this.isReloading;
    }

    IEnumerator gunRecoil() {
        // StartCoroutine(cancelAnim());
        animator.SetBool("shoot", true);
        yield return new WaitForSeconds(0.3f);
        animator.SetBool("shoot", false);
    }

    // IEnumerator cancelAnim() {
    //     animator.SetBool("cancelAnim", true);
    //     yield return new WaitForSeconds(0.05f);
    //     animator.SetBool("cancelAnim", false);
    // }

    IEnumerator hideHitmarker() {
        hitmarker.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        hitmarker.SetActive(false);
    }
}
