using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class WeaponObject : ScriptableObject
{
    public Weapon type;

    public float damage;
    public float fireRate;
    public float actualChagerAmmo;
    public float maxChargerCapacity;
    public float MaxAmmo;

    public float recoilX;
    public float recoilY;
    public float recoilZ;
    public float snappiness;
    public float returnSpeed;

    public AudioClip gunShotSound;
}
