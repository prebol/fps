using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using TMPro;

public class ControlarAvion : MonoBehaviour
{
    public float flySpeed = 5;
    public float turnSpeed = 120;
    private float _yaw;
    public float damage = 100;

    public GameObject planeCamera;
    public ParticleSystem explosionParticle;

    public bool IsDestroyed = false;

    public Vector3 posInicial;

    public Transform spherePosition;
    public float sphereRadius = 5f;
    public LayerMask everythingMask;

    public GameEvent enablePlayerEvent; 

    void Update()
    {
        if (!IsDestroyed) {
            ChangeDirection();
            MoveForward(); 
        }
        
    }

    void ChangeDirection() {
        float moveX = Input.GetAxis("Horizontal");
        float moveY = Input.GetAxis("Vertical");

        _yaw += moveX * turnSpeed * Time.deltaTime;
        float pitch = Mathf.Lerp(0, 20, Mathf.Abs(moveY)) * Mathf.Sign(moveY);

        transform.localRotation = Quaternion.Euler(Vector3.up * _yaw + Vector3.right * pitch);

        // print("MoveX: " + moveX);
        // print("_yaw: " + _yaw);
    }

    void MoveForward() {
        transform.position += transform.forward * flySpeed * Time.deltaTime;
    }

    void OnTriggerEnter(Collider collision)
    {
        print("EL AVION HA CHOCADO, BOOM!");
        IsDestroyed = true;
        explosionParticle.transform.SetParent(null);
        explosionParticle.Play(); 
        enablePlayerEvent.Raise();
        
        RaycastHit[] hits = Physics.SphereCastAll(spherePosition.position, sphereRadius, transform.forward, sphereRadius);
        if (hits != null) {
            foreach (RaycastHit hit in hits) {
                print(hit.transform.tag);
             if (hit.transform.CompareTag("enemy")) {
                 hit.transform.GetComponent<EnemyMov>()?.damage(damage); 
            }
        }
        }

        Destroy(this.gameObject); 
    }

    public void SetVCamPriority(int priority) {
        planeCamera.GetComponent<CinemachineVirtualCamera>().Priority = priority;
    }
}
