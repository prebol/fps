using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public Animator anim;
    public GameObject camera;

    public GameObject gun;
    private Gun _ak47;
    public GameObject planePrefab;

    public float hp = 100;
    public int moveSpeed;

    public int strafeWalkSpeed = 2;     //Walking speed of player while strafing
    public int walkSpeed = 5;       //Running speed
    public int strafeRunSpeed = 4;      //Running speed of player while strafing
    public int runSpeed = 9;    //Running speed


    public float gravity = -9.8f;
    public float jumpForce = 3f;

    //CHARACTER CONTROLLER PARAMETERS
    public float standingHeight = 3.8f;     //Player standing height
    public float crouchHeight = 2.66f;        //Player crouching height
    public float standingCenterY = 0.5f;    //Player center Y standing
    public float crouchCenterY = -0.55f;    //Player center Y crouching

    public float distanceFromStaticToCrouch = 0.8f; //Difference of height between player standing and crouching

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 moveDirection, velocity = Vector3.zero;

    bool isGrounded;
    public bool isRunning;
    public bool isRunningForward = false;
    public bool isCrouching;
    public bool isStrafingLeft; 
    public bool isStrafingRight;
    public bool IsPlaying = true;
    public bool IsMoving;

    public GameEvent disablePlayerEvent;

    public GameObject planeGUI;

    void Start() {
        _ak47 = gun.GetComponent<Gun>();
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0) {
            velocity.y = -2f;
        }

        // if (!isGrounded) {   //quiero que en el aire tenga momentum y no puedas cambiar de direccion tan facilmente
        //     moveSpeed = 2;
        // }
        Gravity();

        if (IsPlaying) {
            Move();
            Jump();
            Run();
            Crouch();
            LanzarAvion();
        } else {
            Idle();
        }
    }

    void Crouch() {
        if (Input.GetKeyDown(KeyCode.LeftControl)) {
            if (isGrounded) {
                if (!isCrouching) {     //Player crouches
                    // print("CROUCH ON"); 
                    isCrouching = true;
                    controller.height = crouchHeight;   //Changes character controller height setting
                    controller.center = new Vector3(controller.center.x, crouchCenterY, controller.center.z);   //Changes character controller center Y setting
                    anim.SetBool("toggleCrouch", true); //Sets 'toggleCrouch' on true on animator
                    camera.transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y - distanceFromStaticToCrouch, camera.transform.position.z);
                } else {    //Player uncrouches
                    // print("CROUCH OFF");
                    isCrouching = false;
                    controller.height = standingHeight;   //Changes character controller height setting
                    controller.center = new Vector3(controller.center.x, standingCenterY, controller.center.z);   //Changes character controller center Y setting
                    anim.SetBool("toggleCrouch", false); //Sets 'toggleCrouch' on false on animator
                    camera.transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y + distanceFromStaticToCrouch, camera.transform.position.z);
                }
            }
            
            
        }
    }

    void Idle() {
        anim.SetBool("Walking", false);
        anim.SetBool("isWalkingAimForward", false);
        anim.SetBool("isWalkingAimBackward", false);
    }

    void LanzarAvion() {
        if (Input.GetKeyDown(KeyCode.Alpha5)) {
            disablePlayerEvent.Raise();

            GameObject plane = Instantiate(planePrefab.gameObject);
            plane.transform.position = this.transform.position;
            plane.transform.position += Vector3.up * 10;
            plane.transform.SetParent(this.transform);
            plane.GetComponent<ControlarAvion>().SetVCamPriority(15);
            plane.gameObject.SetActive(true);
            ChangePlaneGUIVisibility(true);  //Actives canvas object for plane GUI
        }
    }

    public void ChangePlaneGUIVisibility(bool flag) {
        planeGUI.gameObject.SetActive(flag);
    }

    public void DisablePlayer() {
        IsPlaying = false;
    }

    public void EnablePlayer() {
        IsPlaying = true;
        ChangePlaneGUIVisibility(false);
    }

    void Move() {
        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");

        moveDirection = transform.right * moveX + transform.forward * moveZ;

        controller.Move(moveDirection * moveSpeed * Time.deltaTime);
        
        IsMoving = moveX != 0 || moveZ != 0;

        if (moveX != 0 || moveZ != 0) {
            anim.SetBool("Walking", true);
            if (moveZ > 0) {
                isRunningForward = true;
                anim.SetBool("isWalkingAimForward", true);
                anim.SetBool("isWalkingAimBackward", false);
            } else {
                isRunningForward = false;
                anim.SetBool("isWalkingAimBackward", true);
                anim.SetBool("isWalkingAimForward", false);
            }
            
        } else {
            anim.SetBool("Walking", false);
            anim.SetBool("isWalkingAimForward", false);
            anim.SetBool("isWalkingAimBackward", false);
        }
    }

    void Run() {
        if (Input.GetKey(KeyCode.LeftShift)) {  //CORRER
            if (isRunningForward && isGrounded && !_ak47.isShootingFunc() && IsMoving && !_ak47.isReloadingFunc()) { 
                isRunning = true;
                moveSpeed = runSpeed;
                anim.SetBool("Running", true);
            }
        } else {    //ANDAR
            if (isGrounded) {
                
            }
            isRunning = false;
            moveSpeed = walkSpeed;
                
            anim.SetBool("Running", false);
            // print("Running false");
        }
    }

    void Jump() {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded) {
            velocity.y = Mathf.Sqrt(jumpForce * -2f * gravity);
        }
    }

    void Gravity() {
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }
}
